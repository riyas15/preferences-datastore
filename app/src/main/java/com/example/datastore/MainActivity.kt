package com.example.datastore

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import com.example.datastore.ui.theme.DataStoreTheme
import com.example.datastore.ui.theme.ThemeColor
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DataStoreTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    UserData()
                }
            }
        }
    }
}

@Composable
fun UserData() {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        val context = LocalContext.current
        val scope = rememberCoroutineScope()
        val userInfo = SaveUserInfo(context)

        var name by rememberSaveable{ mutableStateOf("")}
        var age by rememberSaveable { mutableStateOf("") }

        Text(
            text = "Hello, ${userInfo.userNameFlow.collectAsState("").value}!",
            modifier = Modifier.padding(10.dp)
        )
        Text(
            text = "Your Age is, ${userInfo.userAgeFlow.collectAsState(0).value}!",
            modifier = Modifier.padding(10.dp)
        )

        OutlinedTextField(value = name,
            onValueChange = { name = it },
            label = { Text("Name") },
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = ThemeColor,
                focusedLabelColor= ThemeColor
            )
        )

        OutlinedTextField(value = age,
            onValueChange = { age = it },
            label = { Text("Age") },
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = ThemeColor,
                focusedLabelColor= ThemeColor
            )
        )


        Button(onClick = {
           scope.launch {
                userInfo.storeUserInfo(age.toInt(), name)
            }
        },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = ThemeColor,
                contentColor = Color.White
            )
        ) {
            Text(text = "Submit data")
        }
    }
}

